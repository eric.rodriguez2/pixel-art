import cv2
import random
PixelT = []
X1 = 50
Y1 = 50
X2 = 60
Y2 = 60

A = [ [0,1,1,0],
      [1,0,0,1],
      [1,1,1,1],
      [1,0,0,1],
      [1,0,0,1]
      ]

B = [ [1,1,1,0],
      [1,0,0,1],
      [1,1,1,0],
      [1,0,0,1],
      [1,1,1,0]
      ]

C = [ [0,1,1,1],
      [1,0,0,0],
      [1,0,0,0],
      [1,0,0,0],
      [0,1,1,1]
      ]

D = [ [1,1,1,0],
      [1,0,0,1],
      [1,0,0,1],
      [1,0,0,1],
      [1,1,1,0]
      ] 

E = [ [1,1,1,1],
      [1,0,0,0],
      [1,1,1,1],
      [1,0,0,0],
      [1,1,1,1]
      ]        

F = [ [1,1,1,1],
      [1,0,0,0],
      [1,1,1,1],
      [1,0,0,0],
      [1,0,0,0]
      ]  

G = [ [1,1,1,1],
      [1,0,0,0],
      [1,0,1,1],
      [1,0,0,1],
      [1,1,1,1]
      ]   

H = [ [1,0,0,1],
      [1,0,0,1],
      [1,1,1,1],
      [1,0,0,1],
      [1,0,0,1]
      ]  

I = [ [1,1,1,1],
      [0,1,1,0],
      [0,1,1,0],
      [0,1,1,0],
      [1,1,1,1]
      ]  

J = [ [1,1,1,1],
      [0,0,0,1],
      [0,0,0,1],
      [1,0,0,1],
      [0,1,1,0]
      ]  

K = [ [1,0,0,1],
      [1,0,1,0],
      [1,1,0,0],
      [1,0,1,0],
      [1,0,0,1]
      ]      

L = [ [1,0,0,0],
      [1,0,0,0],
      [1,0,0,0],
      [1,0,0,0],
      [1,1,1,1]
      ]  

M = [ [1,0,0,1],
      [1,1,1,1],
      [1,1,1,1],
      [1,0,0,1],
      [1,0,0,1], 
      ]     

N  = [ [1,0,0,1],
       [1,1,0,1],
       [1,1,1,1],
       [1,0,1,1],
       [1,0,0,1]
       ]        

O = [ [1,1,1,1],
      [1,0,0,1],
      [1,0,0,1],
      [1,0,0,1],
      [1,1,1,1]
      ] 

P = [ [1,1,1,1],
      [1,0,0,1],
      [1,1,1,1],
      [1,0,0,0],
      [1,0,0,0]
      ]      

Q = [ [1,1,1,1],
      [1,0,0,1],
      [1,0,1,1],
      [1,1,1,1],
      [0,0,0,1]
      ]        

R = [ [1,1,1,1],
      [1,0,0,1],
      [1,1,1,1],
      [1,0,1,0],
      [1,0,0,1]
      ]      

S = [ [0,1,1,1],
      [1,0,0,0],
      [0,1,1,1],
      [0,0,0,1],
      [1,1,1,0]
      ]        

T = [ [1,1,1,1],
      [0,1,1,0],
      [0,1,1,0],
      [0,1,1,0],
      [0,1,1,0]
      ]    

U =[ [1,0,0,1],
     [1,0,0,1],
     [1,0,0,1],
     [1,0,0,1],
     [1,1,1,1]
     ]      

V = [ [1,0,0,1],
      [1,0,0,1],
      [1,0,0,1],
      [1,0,0,1],
      [0,1,1,0]
      ]        

W = [ [1,0,0,1],
      [1,0,0,1],
      [1,1,1,1],
      [1,1,1,1],
      [1,0,0,1]
      ]     

X = [ [1,0,0,1],
      [1,0,0,1],
      [0,1,1,0],
      [1,0,0,1],
      [1,0,0,1]
      ]       

Y = [ [1,0,0,1],
      [1,0,0,1],
      [0,1,1,0],
      [0,1,1,0],
      [0,1,1,0]
      ]

Z = [ [1,1,1,1],
      [0,0,1,0],
      [0,1,0,0],
      [1,0,0,0],
      [1,1,1,1]
      ]
Space = [ [0,0,0,0],
          [0,0,0,0],
          [0,0,0,0],
          [0,0,0,0],
          [0,0,0,0]
        ]
Apostrophe = [[0,0,1,0],
              [0,0,1,0],
              [0,1,0,0],
              [0,0,0,0],
              [0,0,0,0]
            ]  
Interrogation =[[1,1,1,0],
                [0,0,0,1],
                [0,1,1,0],
                [0,0,0,0],
                [0,1,0,0],            
            ]           
Exclamation =[  [0,1,0,0],
                [0,1,0,0],
                [0,1,0,0],
                [0,0,0,0],
                [0,1,0,0],
            ]
Virgule   =  [  [0,0,0,0],
                [0,0,0,0],
                [0,0,1,0],
                [0,0,1,0],
                [0,1,0,0],          
            ]
Point   =  [    [0,0,0,0],
                [0,0,0,0],
                [0,0,0,0],
                [0,1,1,0],
                [0,1,1,0],
            ]   
    

img = cv2.imread("hi.jpg",1)
img = cv2.resize(img,(2000,200))
img2 = cv2.imread("hi.jpg",1)
img2 = cv2.resize(img,(2000,200))

texte = input("Que vouleez vous ecrire ? : ")

for char in texte:
    if char == "A" or char  == "a" or char == "à":
        PixelT = A
    if char == "B" or char  == "b":
        PixelT = B
    if char == "C" or char  == "c":
        PixelT = C
    if char == "D" or  char == "d":
        PixelT = D
    if char == "E" or char == "e" or char == "é" or char == "è":
        PixelT = E 
    if char == "F" or char == "f":
        PixelT = F
    if char == "G" or char == "g":
        PixelT = G
    if char == "H" or char == "h":
        PixelT = H
    if char == "I" or char == "i":
        PixelT = I
    if char == "J" or char == "j":
        PixelT = J
    if char == "K" or char == "k":
        PixelT = K
    if char == "L" or char == "l":
        PixelT = L
    if char == "M" or char == "m":
        PixelT = M
    if char == "N" or char == "n":
        PixelT = N
    if char == "O" or char == "o":
        PixelT = O
    if char == "P" or char == "p":
        PixelT = P
    if char == "Q" or char == "q":
        PixelT = Q
    if char == "R" or char == "r":
        PixelT = R
    if char == "S" or char == "s":
        PixelT = S
    if char == "T" or char == "t":
        PixelT = T
    if char == "U" or char == "u" or char == "ù":
        PixelT = U
    if char == "V" or char == "v":
        PixelT = V
    if char == "W" or char == "w":
        PixelT = W
    if char == "X" or char == "x":
        PixelT = X
    if char == "Y" or char == "y":
        PixelT = Y
    if char == "Z" or char == "z":
        PixelT = Z
    if char == " ":
        PixelT = Space
    if char == "'":
        PixelT = Apostrophe
    if char == "?":
        PixelT = Interrogation
    if char == "!":
        PixelT = Exclamation
    if char == ",":
        PixelT = Virgule
    if char == ".":
        PixelT = Point

    for WrtPixel in PixelT:
        for TestPixel in WrtPixel:
            if TestPixel == 1:
                cv2.rectangle(img, (X1, Y1), (X2, Y2), (random.randint(113,113), random.randint(24,24), random.randint(249,249)), -1)
            X1 = X1 + 10
            X2 = X2 + 10
        X1 = X1 - 40
        X2 = X2 - 40
        Y1 = Y1 + 10
        Y2 = Y2 + 10 
    if PixelT == []:
        pass
    else:
        X1 = X1 + 50
        X2 = X2 + 50
        Y1 = Y1 - 50
        Y2 = Y2 - 50
        PixelT=[]
    cv2.imshow('pixel',img)
    cv2.waitKey(1000)

for i in range(0,10) :
    cv2.imshow('pixel',img)
    cv2.waitKey(1200)
  

